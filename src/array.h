/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/orientation.
 *
 * Orientation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Orientation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Orientation.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef ARRAY_H
#define ARRAY_H

#include <stdlib.h>

#define DEFINE_ARRAY(type)                                                      \
typedef struct {                                                                \
    size_t length;                                                              \
    type *a;                                                                    \
} Array_##type;

#define CALLOC_ARRAY(type, length)                                              \
    { length, calloc(length, sizeof(type)) }

#endif
