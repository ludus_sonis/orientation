/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/orientation.
 *
 * Orientation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Orientation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Orientation.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <mqueue.h>
#include <unistd.h>

#include "graphic.h"
#include "error.h"
#include "mesh.h"

const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

Array_Mesh g_meshes = { 0, 0 };

void (*glfw_terminate) (void) = 0;

void
framebuffer_size_callback (GLFWwindow * window, int width, int height)
{
  glViewport (0, 0, width, height);
}

void
processInput (GLFWwindow * window)
{
  if (glfwGetKey (window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    glfwSetWindowShouldClose (window, 1);
}

GLFWwindow *
drawInit (void)
{
  glfwInit ();
  glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow *window =
    glfwCreateWindow (SCR_WIDTH, SCR_HEIGHT, "Orientation", NULL, NULL);
  if (window == NULL)
    {
      const char *desc;
      glfwGetError (&desc);
      glfwTerminate ();
      DIE ("Failed to create GLFW window: %s", desc);
    }
  glfw_terminate = glfwTerminate;
  glfwMakeContextCurrent (window);
  glfwSetFramebufferSizeCallback (window, framebuffer_size_callback);

  // glad: load all OpenGL function pointers
  // ---------------------------------------
  if (!gladLoadGLLoader ((GLADloadproc) glfwGetProcAddress))
    {
      DIE ("Failed to initialize glad");
    }

  glEnable (GL_DEPTH_TEST);

  GLboolean b;
  glGetBooleanv (GL_SHADER_COMPILER, &b);
  if (GL_FALSE == b)
    DIE ("No shader compiler");

  return window;
}

void
renderLoop (GLFWwindow * window, GLuint shaderProgram, Array_Mesh meshes)
{
  mat4 projection;
  glm_mat4_identity (projection);
  glm_perspective (glm_rad (45.0f), (float) SCR_WIDTH / (float) SCR_HEIGHT,
		   0.1f, 100.0f, projection);
  mat4 view;
  glm_lookat ((vec3)
	      {
	      -30.0f, 0.0f, 0.0f}, (vec3)
	      {
	      0.0f, 0.0f, 0.0f}, (vec3)
	      {
	      0.0f, 0.0f, 1.0f}, view);
  mat4 model;
  glm_mat4_identity (model);
  mat3 rot;
  mat3 rot_todraw;
  glm_mat3_identity (rot);
  while (!glfwWindowShouldClose (window))
    {
      processInput (window);
      glClearColor (1.0f, 1.0f, 1.0f, 1.0f);
      glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      glUseProgram (shaderProgram);

      ssize_t bytes_left = sizeof (rot);
      ssize_t rec;
      bool complete_rot = false;
      while (1)
	{
	  rec =
	    read (0, ((char *) rot) + sizeof (rot) - bytes_left, bytes_left);
	  if (-1 == rec)
	    {
	      if (EAGAIN != errno)
		DIE_ERRNO ("Failed to read");
	      else
		{
		  if (complete_rot)
		    {
		      break;
		    }
		  continue;
		}
	    }
	  if (bytes_left > rec)
	    {
	      bytes_left = bytes_left - rec;
	    }
	  else
	    {

	      bytes_left = sizeof (rot);
	      glm_mat3_copy (rot, rot_todraw);
	      complete_rot = true;
	    }
	}

      glm_mat3_transpose (rot_todraw);
      glm_mat4_ins3 (rot_todraw, model);
      mat4 mvp;
      glm_mat4_mul (projection, view, mvp);
      glm_mat4_mul (mvp, model, mvp);


      GLuint locationMvp = glGetUniformLocation (shaderProgram, "mvp");
      if (-1 == locationMvp)
	DIE ("Failed to get \"mvp\" location");
      glUniformMatrix4fv (locationMvp, 1, GL_FALSE, (GLfloat *) mvp);

      for (unsigned int u = 0; u < meshes.length; ++u)
	drawMesh (shaderProgram, &meshes.a[u]);
      glfwSwapBuffers (window);
      glfwPollEvents ();
    }
}

void
clean_graphics (void)
{
  for (unsigned int u = 0; u < g_meshes.length; ++u)
    {
      meshDelete (&g_meshes.a[u]);
    }
  if (glfw_terminate)
    glfw_terminate ();
}
