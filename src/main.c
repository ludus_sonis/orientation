/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/orientation.
 *
 * Orientation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Orientation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Orientation.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "error.h"
#include "graphic.h"
#include "model.h"
#include "memory.h"
#include "mesh.h"
#include "shader.h"

int
main (int argc, char const *argv[])
{
  GLFWwindow *window = drawInit ();

  GLuint vertexShader =
    sourceAndCompileShader (GL_VERTEX_SHADER, "orientation.vs");
  GLuint fragmentShader =
    sourceAndCompileShader (GL_FRAGMENT_SHADER, "orientation.fs");

  GLuint shaderProgram = createShaderProgram (vertexShader, fragmentShader);
  Array_Mesh meshes = importModel ("axis.obj");
  HEAP_CHUNK_REGISTER (meshes.a, NULL);

  int i = fcntl (0, F_GETFL);
  i = fcntl (0, F_SETFL, i | O_NONBLOCK);
  if (-1 == i)
    DIE_ERRNO ("fcntl F_SETFL");

  renderLoop (window, shaderProgram, meshes);

  clean_before_exit ();

  return 0;
}
