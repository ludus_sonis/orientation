/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/orientation.
 *
 * Orientation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Orientation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Orientation.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef MEMORY_H
#define MEMORY_H

#include <sys/queue.h>
#include <stddef.h>

#include "defs.h"

typedef void (*free_t)(void *);
struct Chunk {
	void *buf;
	free_t free;
	 SLIST_ENTRY(Chunk) entries;
};

void test_alloc(void *buf, const char *vname, const char *funcname);
#define TEST_ALLOC(buf) test_alloc((buf), #buf, __func__)
void *wrap_calloc(size_t nmemb, size_t size, const char *vname,
		  const char *funcname);
#define WRAP_CALLOC(nmemb, size, v) wrap_calloc((nmemb), (size), #v, __func__)
void heapChunkRegister(void *buf, char const *vname, char const *funcname,
		       free_t free_func);
#define HEAP_CHUNK_REGISTER(buf, free_func) heapChunkRegister((buf), #buf, __func__, (free_func))
void *callocAndRegister(size_t nmemb, size_t size, char const *vname,
			char const *funcname);
#define CALLOC_AND_REGISTER(nmemb, size, name) callocAndRegister((nmemb), (size), (name), __func__)

void clean_before_exit(void);

#endif
