/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/orientation.
 *
 * Orientation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Orientation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Orientation.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdio.h>

#include "error.h"
#include "memory.h"

char const* fileImport(char const *filename)
{
	char *buf = NULL;
	long size, rsize;
	FILE *f = fopen(filename, "r");

	if (NULL == f)
		DIE_ERRNO("Failed to open %s\n", filename);
	if (0 != fseek(f, 0, SEEK_END))
		DIE_ERRNO("Failed to fseek %s\n", filename);
	size = ftell(f);
	if (-1 == size)
		DIE_ERRNO("Failed to ftell %s\n", filename);
	if (0 != fseek(f, 0, SEEK_SET))
		DIE_ERRNO("Failed to fseek %s\n", filename);

	buf = WRAP_CALLOC(size+1, sizeof(*buf), buf);

	rsize = fread(buf, sizeof(*buf), size, f);
	if (size != rsize)
		DIE("Failed to fread %s\n", filename);

	if (0 != fclose(f))
		DIE_ERRNO("Failed to fclose %s\n", filename);

	return buf;
}
