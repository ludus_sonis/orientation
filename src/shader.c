/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/orientation.
 *
 * Orientation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Orientation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Orientation.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stddef.h>

#include "import.h"
#include "shader.h"
#include "error.h"

GLuint sourceAndCompileShader(GLenum shaderType, char *filename)
{
	GLchar const* shaderSource = fileImport(filename);

	GLuint shader = glCreateShader(shaderType);
	if (0 == shader)
		DIE("Failed to create shader");
	glShaderSource(shader, 1, &shaderSource, NULL);
	glCompileShader(shader);
	GLint success;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (GL_FALSE == success) {
		GLint infologlength;
		glGetShaderiv(shaderType, GL_INFO_LOG_LENGTH, &infologlength);
		if (0 != infologlength) {
			GLchar infolog[infologlength];
			glGetShaderInfoLog(shader, infologlength, NULL, infolog);
			DIE("Failed to compile shader: %s: %s\n", filename, infolog);
		} else {
			DIE("Failed to compile shader and to get InfoLog: %s", filename);
		}
	}
	return shader;
}

GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader)
{
	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);
	GLint success;
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (GL_FALSE == success) {
		GLuint infologlength;
		glGetProgramiv(vertexShader, GL_INFO_LOG_LENGTH,
			       &infologlength);
		if (0 != infologlength) {
			GLchar infolog[infologlength];
			glGetProgramInfoLog(shaderProgram, infologlength, NULL,
				    infolog);
			DIE("Failed to link shader program: %s\n", infolog);
		} else {
			DIE("Failed to link shader program and to get InfoLog");
		}
	}
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	return shaderProgram;
}
