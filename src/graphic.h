/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/orientation.
 *
 * Orientation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Orientation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Orientation.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef GRAPHIC_H
#define GRAPHIC_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <mqueue.h>

#include "mesh.h"

GLFWwindow *drawInit(void);
void renderLoop(GLFWwindow* window, GLuint shaderProgram, Array_Mesh meshes);
void clean_graphics(void);
extern Array_Mesh g_meshes;
extern void (*glfw_terminate)(void);

#endif
