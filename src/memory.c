/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/orientation.
 *
 * Orientation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Orientation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Orientation.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "error.h"
#include "memory.h"
#include "graphic.h"

void test_alloc(void *buf, const char *vname, const char *funcname)
{
	if (NULL == buf)
		DIE_ERRNO("Failed to alloc %s in %s", vname, funcname);
}

void *wrap_calloc(size_t nmemb, size_t size, const char *vname,
		  const char *funcname)
{
	void *buf = calloc(nmemb, size);
	test_alloc(buf, vname, funcname);
	return buf;
}

SLIST_HEAD(ChunkSlist, Chunk) gHeapChunks = SLIST_HEAD_INITIALIZER(head);

void
heapChunkRegister(void *buf, char const *vname, char const *funcname,
		  free_t free_func)
{
	struct Chunk *chunk = wrap_calloc(1, sizeof(*chunk), vname, funcname);
	chunk->buf = buf;
	if (free_func)
		chunk->free = free_func;
	else
		chunk->free = free;
	SLIST_INSERT_HEAD(&gHeapChunks, chunk, entries);
}

void *callocAndRegister(size_t nmemb, size_t size, char const *vname,
			char const *funcname)
{
	void *buf = wrap_calloc(nmemb, size, vname, funcname);
	heapChunkRegister(buf, vname, funcname, NULL);
	return buf;
}

void clean_before_exit(void)
{
  clean_graphics();

	struct Chunk *chunk;
	while (!SLIST_EMPTY(&gHeapChunks)) {
		chunk = SLIST_FIRST(&gHeapChunks);
		SLIST_REMOVE_HEAD(&gHeapChunks, entries);
		chunk->free(chunk->buf);
		free(chunk);
	}
}

