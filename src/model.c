/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/orientation.
 *
 * Orientation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Orientation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Orientation.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <glad/glad.h>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "model.h"
#include "memory.h"
#include "error.h"
#include "graphic.h"

const struct aiScene *wrap_aiImportFile(const char *file)
{
	const struct aiScene *scene =
	    aiImportFile(file, aiProcessPreset_TargetRealtime_Fast);
	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE
	    || !scene->mRootNode)
		DIE("Error importing scene: %s", aiGetErrorString());
	return scene;
}

void processNode(Array_Mesh meshes, struct aiNode const* node, struct aiScene const *scene)
{
	for (GLuint u = 0; u < node->mNumMeshes; ++u) {
		struct aiMesh const *aimesh = scene->mMeshes[node->mMeshes[u]];
		Mesh *mesh = &meshes.a[node->mMeshes[u]];
		processMesh(mesh, aimesh, scene);
	}
	for (GLuint u = 0; u < node->mNumChildren; ++u)
		processNode(meshes, node->mChildren[u], scene);
}

Array_Mesh importModel(char const *file)
{
	const struct aiScene *scene = wrap_aiImportFile(file);

	Array_Mesh meshes = CALLOC_ARRAY(Mesh, scene->mNumMeshes);
	TEST_ALLOC(meshes.a);
  g_meshes = meshes;

	processNode(meshes, scene->mRootNode, scene);

  aiReleaseImport(scene);

	return meshes;
}
