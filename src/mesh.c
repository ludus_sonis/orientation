/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/orientation.
 *
 * Orientation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Orientation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Orientation.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stddef.h>
#include "mesh.h"
#include "memory.h"
#include "error.h"

#include <stdio.h>
void
meshInit(Mesh * mesh, Array_vec3 vertices, Array_uint indices,
	 vec3 color)
{
	GLuint *pVAO = &mesh->VAO, *pVBO = &mesh->VBO, *pEBO = &mesh->EBO;
  mesh->indicesLength = indices.length;
  glm_vec3_copy(color, mesh->color);

	glGenVertexArrays(1, pVAO);
	glGenBuffers(1, pVBO);
	glGenBuffers(1, pEBO);

	glBindVertexArray(*pVAO);
	glBindBuffer(GL_ARRAY_BUFFER, *pVBO);
	glBufferData(GL_ARRAY_BUFFER, vertices.length * sizeof(*vertices.a),
		     vertices.a, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *pEBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		     indices.length * sizeof(*indices.a), indices.a,
		     GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(*vertices.a), (void*)0);

	glBindVertexArray(0);
}

void drawMesh(GLuint shaderProgram, Mesh* mesh)
{
	glBindVertexArray(mesh->VAO);
  GLint colorLocation = glGetUniformLocation(shaderProgram, "objectColor");
  if (-1 == colorLocation)
    DIE("Failed to get \"objectColor\" location");
  glUniform3fv(colorLocation, 1, mesh->color);
	glDrawElements(GL_TRIANGLES, mesh->indicesLength, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void processMesh(Mesh * mesh, struct aiMesh const* aimesh, struct aiScene const *scene)
{
	Array_vec3 vertices = CALLOC_ARRAY(vec3, aimesh->mNumVertices);
	TEST_ALLOC(vertices.a);

	for (unsigned int u = 0; u < aimesh->mNumVertices; ++u) {
		vec3 *vertex = &vertices.a[u];

		(*vertex)[0] = aimesh->mVertices[u].x;
		(*vertex)[1] = aimesh->mVertices[u].y;
		(*vertex)[2] = aimesh->mVertices[u].z;
	}

  GLuint I = 0;
	for (unsigned int u0 = 0; u0 < aimesh->mNumFaces; ++u0)
    I += aimesh->mFaces[u0].mNumIndices;

	Array_uint indices = CALLOC_ARRAY(uint, I);
	TEST_ALLOC(indices.a);

  I = 0;
	for (unsigned int u0 = 0; u0 < aimesh->mNumFaces; ++u0) {
		struct aiFace face = aimesh->mFaces[u0];
		for (unsigned int u1 = 0; u1 < face.mNumIndices; ++u1) {
			indices.a[I] = face.mIndices[u1];
      ++I;
    }
	}

	struct aiMaterial *material = scene->mMaterials[aimesh->mMaterialIndex];
	struct aiColor4D aicolor = { 0, 0, 0, 0 };
	aiGetMaterialColor(material, AI_MATKEY_COLOR_DIFFUSE, &aicolor);
  vec3 color = { aicolor.r, aicolor.g, aicolor.b };

	meshInit(mesh, vertices, indices, color);

  free(vertices.a);
  free(indices.a);
}

void
meshDelete(Mesh* mesh)
{
  glDeleteVertexArrays(1, &mesh->VAO);
  glDeleteBuffers(1, &mesh->VBO);
  glDeleteBuffers(1, &mesh->EBO);
}
