/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/orientation.
 *
 * Orientation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Orientation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Orientation.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef ERROR_H
#define ERROR_H

#include <errno.h>
#include <error.h>
#include <stdlib.h>

#include "memory.h"

#define DIE_AUX(errno, fstring, ...) do {                                           \
  clean_before_exit();                                                              \
  error_at_line(EXIT_FAILURE, errno, __FILE__, __LINE__, fstring, ## __VA_ARGS__);  \
} while(0)
#define DIE_ERRNO(fstring, ...) DIE_AUX(errno, fstring, ## __VA_ARGS__)
#define DIE(fstring, ...) DIE_AUX(0, fstring, ## __VA_ARGS__)

#endif
