/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/orientation.
 *
 * Orientation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Orientation is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Orientation.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef MESH_H
#define MESH_H

#include <glad/glad.h>
#include <assimp/mesh.h>
#include <assimp/scene.h>
#include <cglm/cglm.h>

#include "defs.h"

typedef struct {
	GLuint VBO;
	GLuint VAO;
	GLuint EBO;
  vec3 color;
  GLuint indicesLength;
} Mesh;

DEFINE_ARRAY(Mesh)

void meshInit(Mesh * mesh, Array_vec3 vertices, Array_uint indices,
	      vec3 color);
void processMesh(Mesh * mesh, struct aiMesh const* aimesh,
		 struct aiScene const *scene);
void drawMesh(GLuint shaderProgram, Mesh* mesh);
void meshDelete(Mesh* mesh);

#endif
